export interface CoinGecko {
  id: string,
  symbol: string,
  name: string
}
