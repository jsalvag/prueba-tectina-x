import {CoinLore} from "./coin-lore";

export interface CoinLoreResponse {
  data: CoinLore[];
  info: {
    coins_num: number;
    time: number;
  }
}
