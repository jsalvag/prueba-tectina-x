import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContentComponent} from "./component/content.component";
import {CartComponent} from "./component/cart.component";
import {CartGuard} from "./guard/cart.guard";
import {LogInComponent} from "./component/log-in.component";
import {AuthGuard} from "./guard/auth.guard";

const routes: Routes = [
  {path: '', component: ContentComponent, canActivate: [AuthGuard]},
  {path: 'cart', component: CartComponent, canActivate: [CartGuard, AuthGuard]},
  {path: 'login', component: LogInComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'},  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
