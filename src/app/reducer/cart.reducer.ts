import {createReducer, on} from "@ngrx/store";
import {Action} from "@ngrx/store/src/models";
import {addCoin, changeCoinAmount, removeCoin} from "../action/cart.actions";
import {CoinLore} from "../interfaces/coin-lore";

export const initialState: ReadonlyArray<CoinLore> = []

const _cartReducer = createReducer(
  initialState,
  on(addCoin, (state, {coin}) => {
    if (state.indexOf(coin) > -1) return state;
    return [...state, coin];
  }),
  on(removeCoin, (state, {coin}) => state.filter(c => c.id !== coin.id)),
  on(changeCoinAmount, (state, {coin_id, amount}) => state.map(c => {
    if (c.id === coin_id) {
      return {...c, amount};
    }
    return c;
  }))
)

export function cartReducer(state: ReadonlyArray<CoinLore> | undefined, action: Action) {
  return _cartReducer(state, action);
}
