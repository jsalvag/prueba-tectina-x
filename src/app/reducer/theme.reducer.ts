import {Action, createReducer, on} from "@ngrx/store";
import {toggleTheme} from "../action/theme.action";

export const initialState: boolean = true;

const _themeReducer = createReducer(
  initialState,
  on(toggleTheme, (state) => {
    document.body.classList.toggle('dark');
    return !state;
  })
)

export function themeReducer(state: boolean | undefined, action: Action) {
  return _themeReducer(state, action);
}
