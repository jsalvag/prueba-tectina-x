import {createReducer, on} from '@ngrx/store';
import {loadSessions, logIn, logOut} from "../action/session.actions";


export const sessionFeatureKey = 'session';

export interface SessionState {
  username: string | undefined;
  issued: Date | undefined;
  expire: Date | undefined;
}

export const initialState: SessionState = {
  username: undefined,
  issued: undefined,
  expire: undefined
};


export const sessionReducer = createReducer(
  initialState,
  on(logIn, (state, {username}) => {
    console.log(state, username)
    let now = new Date();
    let session = {
      ...state,
      username,
      issued: now,
      expire: (new Date(1000 * 60 * 60 + now.getTime()))
    };
    sessionStorage.setItem(sessionFeatureKey, JSON.stringify(session));
    return session
  }),
  on(logOut, (state) => {
    let item = sessionStorage.getItem(sessionFeatureKey);
    if (item) {
      sessionStorage.removeItem(sessionFeatureKey);
      return initialState;
    }
    return state;
  }),
  on(loadSessions, (state) => state)
);

