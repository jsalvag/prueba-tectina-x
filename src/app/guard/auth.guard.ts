import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from "@ngrx/store";
import {SessionState} from "../reducer/session.reducer";
import {map, tap} from "rxjs/operators";
import {logOut} from "../action/session.actions";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(private store: Store<{ session: SessionState }>, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(({session}) => session)
      .pipe(
        map(s => {
          let haveUser = !!s.username;
          let expiration = s.expire?.getTime() || 0;
          let isNotExpired = expiration > new Date().getTime();
          console.log({s, haveUser, expiration, isNotExpired})
          return haveUser && isNotExpired;
        }),
        tap(r => {
          console.log(r)
          if (!r){
            this.store.dispatch(logOut());
            this.router.navigate(['/login'], {
              queryParams: {
                return: state.url
              }
            }).then(console.log);
          }
        })
      );
  }

}
