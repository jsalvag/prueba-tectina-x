import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from "@ngrx/store";
import {CoinLore} from "../interfaces/coin-lore";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CartGuard implements CanActivate {
  public coins$ = this.store.select(state => state.cart);

  constructor(private store: Store<{ cart: CoinLore[] }>, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.coins$.pipe(map(coins => {
      if (coins.length > 0) {
        return true;
      }
      this.router.navigate(['/']);
      return false;
    }));
  }

}
