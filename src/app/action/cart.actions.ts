import {createAction, props} from "@ngrx/store";
import {CoinLore} from "../interfaces/coin-lore";

export const addCoin = createAction('[Cart] Add Coin to Cart', props<{ coin: CoinLore }>());
export const removeCoin = createAction('[Cart] Remove Coin from Cart', props<{ coin: CoinLore }>());
export const changeCoinAmount = createAction('[Cart] Change Coin Amount', props<{ coin_id?: string, amount: number }>());
