import {createAction, props} from '@ngrx/store';

export const logIn = createAction(
  '[Session] Log In User', props<{ username: string }>()
);

export const logOut = createAction(
  '[Session] Log Out User'
);

export const loadSessions = createAction(
  '[Session] Load Sessions'
);
