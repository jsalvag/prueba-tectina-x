import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="pt-5">
      <app-header></app-header>
      <router-outlet></router-outlet>
    </div>`,
  styles: []
})
export class AppComponent {

  constructor() {
  }
}
