import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CoinGecko} from "../interfaces/coinGecko";

@Injectable({
  providedIn: 'root'
})
export class CoinGeckoService {
  static BASE = 'https://api.coingecko.com/api/v3/'
  private ENDPOINTS = {
    coins: {
      list: () => CoinGeckoService.BASE + 'coins/list'
    }
  }

  constructor(private http: HttpClient) {}

  ping(): Observable<any> {
    return this.http.get('https://api.coingecko.com/api/v3/ping')
  }

  list(): Observable<CoinGecko[]> {
    return this.http.get<CoinGecko[]>(this.ENDPOINTS.coins.list())
  }
}
