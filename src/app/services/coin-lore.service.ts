import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CoinLoreResponse} from "../interfaces/coin-lore-response";

@Injectable({
  providedIn: 'root'
})
export class CoinLoreService {
  static BASE = 'https://api.coinlore.net/api/'
  private ENDPOINTS = {
    coins: {
      list: (params?: { start?: number, limit?: number }) => {
        let stringParams;
        if (params) {
          // @ts-ignore
          stringParams = Object.keys(params).fill(key => params[key] >= 0).map(key => `${key}=${params[key]}`).join('&')
        }
        return `${CoinLoreService.BASE}tickers/${(stringParams ? `?${stringParams}` : '')}`;
      }
    }
  }

  constructor(private http: HttpClient) {
  }

  list(params?: { start?: number, limit?: number }): Observable<CoinLoreResponse> {
    return this.http.get<CoinLoreResponse>(this.ENDPOINTS.coins.list(params))
  }
}
