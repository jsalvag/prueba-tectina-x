import {Component, OnInit} from '@angular/core';
import {faMoon, faToggleOff, faToggleOn} from "@fortawesome/free-solid-svg-icons";
import {faSun} from "@fortawesome/free-regular-svg-icons";
import {Store} from "@ngrx/store";
import {toggleTheme} from "../action/theme.action";
import {SessionState} from "../reducer/session.reducer";
import {logOut} from "../action/session.actions";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  template: `
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
      <div class="container">
        <a class="navbar-brand" href="#">My Store App</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="" data-bs-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarColor01">
          <ng-container *ngIf="(session$ | async) as session">
            <ng-container *ngIf="session.username">
              <ul class="navbar-nav me-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#" routerLink="/" routerLinkActive="active">Coins</a>
                </li>
                <li class="nav-item dropdown">
                  <app-shopping-car-button></app-shopping-car-button>
                </li>
              </ul>
              <div class="di-flex row">
                <button class="btn text-light w-100 col" type="button" (click)="toggleTheme()"><span class="d-flex flex-row">
                  <fa-icon [icon]="sun"></fa-icon>&nbsp;
                  <fa-icon [icon]="(theme$ | async) ? toggleOff : toggleOn"></fa-icon>&nbsp;
                  <fa-icon [icon]="moon"></fa-icon>
                </span></button>
                <ul class="navbar-nav me-auto col">
                  <li ngbDropdown class="nav-item active">
                    <a href (click)="false" class="nav-link" ngbDropdownToggle>
                      {{session.username}}
                    </a>
                    <div ngbDropdownMenu>
                      <button ngbDropdownItem (click)="logOut()">Log Out</button>
                    </div>
                  </li>
                </ul>
              </div>
            </ng-container>
          </ng-container>
        </div>
      </div>
    </nav>
  `,
  styles: []
})
export class HeaderComponent implements OnInit {
  public toggleOn = faToggleOn;
  public toggleOff = faToggleOff;
  public sun = faSun;
  public moon = faMoon;
  public theme$ = this.store.select(({theme}) => theme);
  public session$ = this.store.select(({session}) => session);

  constructor(private store: Store<{ theme: boolean, session: SessionState }>,
              private router: Router) {
  }

  ngOnInit(): void {

  }

  toggleTheme() {
    this.store.dispatch(toggleTheme())
  }

  logOut() {
    this.store.dispatch(logOut())
    this.router.navigate(['/login'],);

  }
}
