import {Component, OnInit} from '@angular/core';
import {faSpinner} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-loading',
  template: `
    <div class="w-100 h-100 d-flex justify-content-center align-items-center flex-column">
      <span>Loading...</span>
      <fa-icon [icon]="spinner" [spin]="true" size="3x"></fa-icon>
    </div>
  `,
  styles: [`
    div {
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      span{
        font-size: 3rem;
      }
    }
  `]
})
export class LoadingComponent implements OnInit {
  public spinner = faSpinner

  constructor() {
  }

  ngOnInit(): void {
  }

}
