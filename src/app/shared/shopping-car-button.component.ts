import {Component, OnInit} from '@angular/core';
import {faShoppingCart, faSpinner, faTimes} from "@fortawesome/free-solid-svg-icons";
import {CoinLore} from "../interfaces/coin-lore";
import {Store} from "@ngrx/store";
import {removeCoin} from "../action/cart.actions";

@Component({
  selector: 'app-shopping-car-button',
  template: `
    <div class="col">
      <div class="nav-item" ngbDropdown display="dynamic" placement="bottom-left">
        <a class="nav-link text-light" tabindex="0" ngbDropdownToggle id="store-app-navbar" role="button">
          <fa-icon [icon]="cartIcon"></fa-icon>
        </a>
        <ul ngbDropdownMenu aria-labelledby="store-app-navbar" class="dropdown-menu dropdown-menu-left">
          <ng-container *ngIf="cart$ | async as coins; else loading">
            <ng-container *ngIf="coins.length; else noCoinsSelected">
              <li class="dropdown-item-text d-flex justify-content-between" *ngFor="let coin of coins">
                <div class="">
                  <img [src]="coin.nameid | urlImage" [attr.alt]="coin.nameid">
                  {{coin.name}}</div>
                <button class="btn btn-sm btn-link text-danger" (click)="remove(coin)">
                  <fa-icon [icon]="timesIcon"></fa-icon>
                </button>
              </li>
              <div class="dropdown-divider"></div>
              <a ngbDropdownItem href="#" (click)="$event.preventDefault()" routerLink="cart">Show Cart</a>
            </ng-container>
          </ng-container>
        </ul>
      </div>
    </div>
    <ng-template #loading>
      Cargando monedas ...
      <fa-icon [icon]="spinner"></fa-icon>
    </ng-template>
    <ng-template #noCoinsSelected>
      <li class="dropdown-item-text">
        El carrito está vacío
      </li>
    </ng-template>
  `,
  styles: [`.dropdown-menu {
    max-height: 20em;
    width: 15rem;
  }`]
})
export class ShoppingCarButtonComponent implements OnInit {
  public cartIcon = faShoppingCart;
  public timesIcon = faTimes;
  public spinner = faSpinner
  public cart$ = this.store.select(state => state.cart);

  constructor(private store: Store<{ cart: CoinLore[] }>) {
  }

  ngOnInit(): void {
  }

  remove(coin: CoinLore) {
    this.store.dispatch(removeCoin({coin}))
  }

}
