import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './shared/header.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ShoppingCarButtonComponent} from './shared/shopping-car-button.component';
import {ContentComponent} from './component/content.component';
import {HttpClientModule} from "@angular/common/http";
import {UrlImagePipe} from './pipes/url-image.pipe';
import {StoreModule} from "@ngrx/store";
import {cartReducer} from "./reducer/cart.reducer";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {SearchPipe} from './pipes/search.pipe';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {themeReducer} from "./reducer/theme.reducer";
import {CartComponent} from './component/cart.component';
import {CoinComponent} from './component/coin.component';
import {LoadingComponent} from './shared/loading.component';
import {CoinDetailsModalComponent} from './component/coin-details-modal.component';
import {CoinsTotalPipe} from './pipes/coins-total.pipe';
import {LogInComponent} from './component/log-in.component';
import {sessionReducer} from "./reducer/session.reducer";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShoppingCarButtonComponent,
    ContentComponent,
    UrlImagePipe,
    SearchPipe,
    CartComponent,
    CoinComponent,
    LoadingComponent,
    CoinDetailsModalComponent,
    CoinsTotalPipe,
    LogInComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    StoreModule.forRoot({
      cart: cartReducer,
      theme: themeReducer,
      session: sessionReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 4
    }),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
