import {Pipe, PipeTransform} from '@angular/core';
import {CoinLore} from "../interfaces/coin-lore";

@Pipe({
  name: 'coinsTotal'
})
export class CoinsTotalPipe implements PipeTransform {

  transform(coins: CoinLore[] | undefined): number {
    if (coins) {
      return coins.reduce((total, coin) => (total += coin.price_usd * (coin.amount || 1)), 0)
    }
    return 0;
  }

}
