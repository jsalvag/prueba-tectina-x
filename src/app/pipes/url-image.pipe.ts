import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'urlImage'
})
export class UrlImagePipe implements PipeTransform {

  transform(nameid: string, size: string = '25x25/'): string {
    return `https://www.coinlore.com/img/${size}${nameid}.png`;
  }

}
