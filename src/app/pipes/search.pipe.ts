import {Pipe, PipeTransform} from '@angular/core';
import {CoinLore} from "../interfaces/coin-lore";

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  public transform(value: CoinLore[], keys: string, term: string) {

    if (!term) return value;
    // @ts-ignore
    return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
  }

}
