import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {CoinLore} from "../interfaces/coin-lore";

@Component({
  selector: 'app-cart',
  template: `
    <div class="container">
      <h1 class="mt-3">{{title}}</h1>
      <ng-container *ngIf="coins$ | async as coins; else loading">
        <div class="row row-cols-xl-5 row-cols-lg-3 row-cols-md-2 row-cols-sm-1 gy-2">
          <div class="col top-5" *ngFor="let coin of coins">
            <app-coin [coin]="coin" [showAddToCart]="false" [showRemoveFromCart]="true" [showAmountInput]="true"></app-coin>
          </div>
        </div>
        <hr><hr>
        Total: {{ coins | coinsTotal | currency }}
      </ng-container>
    </div>
    <ng-template #loading>
      <app-loading></app-loading>
    </ng-template>
  `,
  styles: []
})
export class CartComponent implements OnInit {
  public coins$ = this.store.select(state => state.cart);
  public title = 'Coins Shopping Cart';
  constructor(private store: Store<{ cart: CoinLore[] }>) {
  }

  ngOnInit(): void {
  }

}
