import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Store} from "@ngrx/store";
import {SessionState} from "../reducer/session.reducer";
import {logIn} from "../action/session.actions";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-log-in',
  template: `
    <div class="container bg-primary d-flex justify-content-center align-items-center">
      <div class="card">
        <div class="card-header">
          Sign In
        </div>
        <form [formGroup]="form">
          <div class="card-body">
            <input type="text" placeholder="Username" class="form-control-lg w-100" formControlName="username">
          </div>
          <div class="card-footer">
            <button class="btn btn-primary btn-lg w-100" (click)="logIn()">Sign In</button>
          </div>
        </form>
      </div>
    </div>
  `,
  styles: [`
    div.container {
      position: absolute;
      right: 0;
      top: 0;
      left: 0;
      bottom: 0;

      .card {
        height: 12rem;
      }
    }
  `]
})
export class LogInComponent implements OnInit {
  public form: FormGroup;
  private return = '';

  constructor(private fb: FormBuilder, private store: Store<{ session: SessionState }>,
              private router: Router,
              private route: ActivatedRoute) {
    this.form = this.fb.group({
      username: this.fb.control('', [Validators.required, Validators.minLength(3)])
    })
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => this.return = params['return'] || '')
  }

  logIn() {
    console.log(this.form.value, this.return)
    if (this.form.value) {
      let username = this.form.get('username')?.value;
      console.log(username)
      this.store.dispatch(logIn(this.form.value));
      this.router.navigateByUrl(this.return).then(console.log)
    }
  }
}
