import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {CoinLore} from "../interfaces/coin-lore";

@Component({
  selector: 'app-coin-details-modal',
  template: `
    <ng-container *ngIf="coin; else noCoin">
      <div class="d-flex flex-column">
        <div class="modal-header">
          <div class="d-flex justify-content-center align-items-center">
            <img [src]="coin.nameid | urlImage: ''" [attr.alt]="coin.nameid">
          </div>
          <h4 class="modal-title w-100" id="modal-title">
            <h2 class="card-title p-1">
              {{coin.name}}
            </h2>
          </h4>
        </div>
      </div>
      <div class="modal-body">
        <div class="d-flex flex-wrap">
          <div class="card">
            <div class="card-header">
              Market Cap
            </div>
            <div class="card-body">
              <h5 class="card-title">{{coin.market_cap_usd | currency}}</h5>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" (click)="modal.close('Ok click')">Ok</button>
      </div>
    </ng-container>
    <ng-template #noCoin>
      <div class="modal-body">No hay moneda seleccionada</div>
    </ng-template>`
})
export class CoinDetailsModalComponent implements OnInit {
  @Input() coin: CoinLore | undefined;

  constructor(public modal: NgbActiveModal) {
  }

  ngOnInit(): void {
    if (this.coin) {
      this.coin.amount = 1;
    }
    console.log(this.coin)
  }

}
