import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {CoinLoreService} from "../services/coin-lore.service";
import {CoinLoreResponse} from "../interfaces/coin-lore-response";

@Component({
  selector: 'app-content',
  template: `
    <div class="container">
      <h1 class="mt-3">{{title}}</h1>
      <ng-container *ngIf="coins$ | async as coins; else loading">
        <h4>Top 5</h4>
        <div class="row row-cols-xl-5 row-cols-lg-3 row-cols-md-2 row-cols-sm-1 gy-2">
          <div class="col top-5" *ngFor="let coin of coins.data | slice: 0: 5">
            <app-coin [coin]="coin"></app-coin>
          </div>
        </div>
        <hr>
        <input type="text" [(ngModel)]="query" class="form-control-lg w-100" placeholder="Search coin">
        <hr>
        <div class="row row-cols-xl-4 row-cols-lg-3 row-cols-md-2 row-cols-sm-2 g-4">
          <div class="col"
               *ngFor="let coin of coins.data | slice: 6 | search: 'name,nameid,price_usd,price_btc' : query">
            <app-coin [coin]="coin" [template]="'small'"></app-coin>
          </div>
        </div>
      </ng-container>
    </div>
    <ng-template #loading>
      <app-loading></app-loading>
    </ng-template>
  `
})
export class ContentComponent implements OnInit {
  public title = 'Coin Store App';
  public coins$ = new Observable<CoinLoreResponse>();
  public query = '';

  constructor(private coinService: CoinLoreService) {
  }

  ngOnInit(): void {
    this.coins$ = this.coinService.list();
  }
}
