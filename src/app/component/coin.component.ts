import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {CoinLore} from "../interfaces/coin-lore";
import {addCoin, changeCoinAmount, removeCoin} from "../action/cart.actions";
import {faCartPlus, faInfoCircle, faMinusCircle} from "@fortawesome/free-solid-svg-icons";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CoinDetailsModalComponent} from "./coin-details-modal.component";
import {FormControl} from "@angular/forms";
import {debounceTime, takeUntil} from "rxjs/operators";
import {Subject, Subscription} from "rxjs";

@Component({
  selector: 'app-coin',
  template: `
    <ng-container *ngIf="coin" [ngSwitch]="template">
      <ng-container *ngSwitchCase="'big'">
        <div class="card">
          <div class="coin-icon-container">
            <img [src]="coin.nameid | urlImage: ''" [attr.alt]="coin.nameid">
          </div>
          <div class="card-body">
            <h5 class="card-title">{{coin.name}} <span class="badge fa-pull-right bg-primary">{{coin.rank}}</span>
            </h5>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">U$D: {{coin.price_usd | currency}}</li>
            <li class="list-group-item">BTC: {{coin.price_btc | currency}}</li>
            <li class="list-group-item" *ngIf="showAmountInput">
              <div class="input-group">
                <span class="input-group-text" id="coins-amount">Amount</span>
                <input type="number" class="form-control" placeholder="Amount" aria-label="Amount" min="0"
                       aria-describedby="coins-amount" [formControl]="amount">
              </div>
            </li>
          </ul>
          <div class="card-body d-flex justify-content-between">
            <div class="btn-group">
              <button *ngIf="showAddToCart" type="button" class="btn btn-primary" (click)="addToCart(coin)">
                Add
                <fa-icon [icon]="cartPlus"></fa-icon>
              </button>
              <button *ngIf="showRemoveFromCart" type="button" class="btn btn-danger"
                      (click)="removeFromCart(coin)">
                Remove
                <fa-icon [icon]="minus"></fa-icon>
              </button>
            </div>
            <button type="button" role="button" class="btn btn-link" (click)="showCoinDetails(coin)">
              <fa-icon [icon]="infoIcon"></fa-icon>
            </button>
          </div>
        </div>
      </ng-container>
      <ng-container *ngSwitchCase="'small'">
        <div class="card">
          <div class="card-header">
            <div class="float-end mt-1">
              <button type="button" role="button" class="btn btn-sm btn-link" (click)="showCoinDetails(coin)">
                <fa-icon [icon]="infoIcon"></fa-icon>
              </button>
              <span class="badge bg-info">{{coin.rank}}</span>
            </div>
            <h5 class="card-title p-1">
              <img [src]="coin.nameid | urlImage" [attr.alt]="coin.nameid">
              {{coin.name}}
            </h5>
          </div>
          <div class="row g-0">
            <div class="col-8">
              <ul class="list-group list-group-flush ">
                <li class="list-group-item"><small>U$D: {{coin.price_usd | currency}}</small></li>
                <li class="list-group-item"><small>BTC: {{coin.price_btc | currency}}</small></li>
              </ul>
            </div>
            <div class="col-4 d-flex justify-content-center">
              <button *ngIf="showAddToCart" type="button" class="btn btn-link" (click)="addToCart(coin)">
                <fa-icon [icon]="cartPlus" size="2x"></fa-icon>
              </button>
              <button *ngIf="showRemoveFromCart" type="button" class="btn btn-link text-danger"
                      (click)="removeFromCart(coin)">
                <fa-icon [icon]="minus" size="2x"></fa-icon>
              </button>
            </div>
          </div>
        </div>
      </ng-container>
    </ng-container>
  `,
  styles: [`
    .coin-icon-container {
      height: 6rem;
      background-color: #e2e2e2;
      background-repeat: no-repeat;
      background-position: center center;
      background-size: 2rem;
      display: flex;
      justify-content: center;
      align-items: center;
      img{
        width: 5rem;
      }
    }

    @media screen and (max-width: 576px) {
      .col.top-5 {
        min-width: 18rem;
      }
    }
  `]
})
export class CoinComponent implements OnInit, OnDestroy {
  @Input() template: 'small' | 'big' = 'big';
  @Input() coin: CoinLore | undefined;
  @Input() showAddToCart = true;
  @Input() showRemoveFromCart = false;
  @Input() showAmountInput = false;
  public cartPlus = faCartPlus;
  public minus = faMinusCircle;
  public infoIcon = faInfoCircle;
  public amount: FormControl = new FormControl(1);
  private subscriptions = new Subject();

  constructor(private store: Store<{ cart: CoinLore[] }>, private modalService: NgbModal) {
  }

  private subscription: Subscription | undefined;

  ngOnInit(): void {
    this.amount = new FormControl(this.coin ? this.coin.amount || 1 : 1);
    this.subscription = this.amount.valueChanges.pipe(takeUntil(this.subscriptions), debounceTime(500)).subscribe(amount => {
      if (this.amount) {
        this.amount.setValue(amount, {emitEvent: false});
      }
      this.store.dispatch(changeCoinAmount({coin_id: this.coin?.id, amount}));
    });
  }

  addToCart(coin: CoinLore) {
    this.store.dispatch(addCoin({coin}))
  }

  removeFromCart(coin: CoinLore) {
    this.store.dispatch(removeCoin({coin}))
  }

  showCoinDetails(coin: CoinLore) {
    let ngbModalRef = this.modalService.open(CoinDetailsModalComponent);
    ngbModalRef.componentInstance.coin = coin;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscriptions.unsubscribe();
    }
  }
}
